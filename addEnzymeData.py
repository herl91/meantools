#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import sys
import argparse
import pandas as pd
import numpy as np

import gizmos


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('prediction_transition_matches_file',
                        help='Output from heraldPathways.py')
    parser.add_argument('gene_annotation_file',
                        help='Two-column csv. Gene, annotations.')
    parser.add_argument('output_folder')
    parser.add_argument('--pfam_RR_annotation_file',
                        default='',
                        required=False,
                        help='Nine-column csv. reaction_id, uniprot_id, Pfams, KO, rhea_id_reaction, kegg_id_reaction, '
                             'rhea_confirmation, kegg_confirmation, KO_prediction.')
    parser.add_argument('--pfam_RR_relationship_file',
                        default='',
                        required=False,
                        help='Four-column csv. pfam_acc, pfam_name, reaction_id, num_pfam_reaction')
    parser.add_argument('--max_pfam_per_RR',
                        default=10,
                        required=False,
                        type=int,
                        help='Default: 10.')
    parser.add_argument('--coexpression_file',
                        default='',
                        required=False,
                        help='Four column CSV: metabolite, gene, correlation_coefficient, P-value.')
    parser.add_argument('--gene_expression_file',
                        default='',
                        required=False,
                        help='RPKM. Genes in rows, samples in columns.')
    parser.add_argument('--ms_abundance_file',
                        default='',
                        required=False,
                        help='MS in rows, samples in columns.')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        required=False)
    return parser.parse_args()


#################
# MAIN PIPELINE #
#################
def main():
    # OUTPUT INIT
    pfam_dict_file = os.path.join(sys.path[0], 'pfams_dict.csv')     # Acc,Name,Desc
    gizmos.log_init(Options)

    # CHECKS
    if not Options.coexpression_file:
        if not (Options.gene_expression_file and Options.ms_abundance_file):
            gizmos.print_milestone('Input a coexpression file or both gene_expression and ms_abundance.', True)
            return
    if bool(Options.pfam_RR_annotation_file) == bool(Options.pfam_RR_relationship_file):
        gizmos.print_milestone('Input any ONE of the two Pfam-RR file options.', True)
        return

    # INPUT
    # prediction_transitions_matches
    # ms_substrate, ms_product, expected_mass_transition, predicted_mass_transition, mass_transition_difference
    # predicted_substrate_id, predicted_product_id, reaction_id, substrate_id, product_id, substrate_mnx, product_mnx,
    # smarts_id, predicted_structure_smiles, predicted_product_smiles, RR_substrate_smarts, RR_product_smarts
    gizmos.print_milestone('Loading prediction-transitions...', Options.verbose)
    predictions_df = pd.read_csv(Options.prediction_transition_matches_file, index_col=None, dtype={'reaction_id': str,
                                                                                                    'substrate_id': str,
                                                                                                    'product_id': str})
    predictions_df['reaction_substrate'] = predictions_df.reaction_id + '_' + predictions_df.substrate_id

    # gene annotations
    # gene, pfam
    gizmos.print_milestone('Loading gene annotations...', Options.verbose)
    annotations_df = pd.read_csv(Options.gene_annotation_file, index_col=None)
    annotations_df = annotations_df.rename(columns={annotations_df.columns[0]: 'gene',
                                                    annotations_df.columns[1]: 'enzyme_pfams'})
    enzyme_pfams_list = annotations_df.enzyme_pfams.apply(gizmos.pd_to_list, separator=';')
    # expand gene annotations so there's one pfam per line (but we keep the "pfam" annotation that have them all
    # todo We can abvoid expansion if we devise a method to merge through intersection of pfam domains.
    lens = [len(item) for item in enzyme_pfams_list]
    new_df = pd.DataFrame({'gene': np.repeat(annotations_df.gene, lens),
                           'pfam_rule': np.concatenate(enzyme_pfams_list)})
    annotations_df = pd.merge(annotations_df, new_df, how='outer')
    del enzyme_pfams_list, new_df

    # pfam_RR_file
    gizmos.print_milestone('Loading pfams...', Options.verbose)
    if Options.pfam_RR_relationship_file:       # TODO CHECK.....
        # pfam_number, pfam_name, reaction_id, num_pfam_reaction
        pfam_rules_df = pd.read_csv(Options.pfam_RR_relationship_file, index_col=None)
        pfam_rules_df = pfam_rules_df.rename(columns={pfam_rules_df.columns[0]: 'pfam_acc_rule',
                                                      pfam_rules_df.columns[1]: 'pfam_rule',
                                                      pfam_rules_df.columns[2]: 'reaction_id',
                                                      pfam_rules_df.columns[3]: 'pfams_in_reaction'})
        pfam_rules_df['reaction_id'] = pfam_rules_df.reaction_id.astype('str')
    else:
        # reaction_id, uniprot_id, Pfams, KO, rhea_id_reaction, kegg_id_reaction, rhea_confirmation, kegg_confirmation,
        #  KO_prediction
        pfam_rules_df = pd.read_csv(Options.pfam_RR_annotation_file, index_col=None, dtype={'rhea_id_reaction': str})
        pfam_rules_df = pfam_rules_df.rename(columns={pfam_rules_df.columns[1]: 'uniprot_id',
                                                      pfam_rules_df.columns[2]: 'uniprot_enzyme_pfams_acc'})
        pfam_rules_df['reaction_id'] = pfam_rules_df.reaction_id.astype('str')

        # convert pfam_acc to pfam
        pfam_dict = pd.read_csv(pfam_dict_file, index_col=None)
        pfam_dict.index = pfam_dict.Acc.apply(lambda x: x.split('.')[0])       # Acc,Name,Desc

        uniprot_enzyme_pfams_acc_list = pfam_rules_df.uniprot_enzyme_pfams_acc.apply(gizmos.pd_to_list, separator=' ')
        pfam_rules_df['uniprot_enzyme_pfams_list'] = [[pfam_dict.Name.loc[k] for k in row if k in pfam_dict.index]
                                                      for row in uniprot_enzyme_pfams_acc_list]
        pfam_rules_df['uniprot_enzyme_pfams'] = pfam_rules_df.uniprot_enzyme_pfams_list.apply(';'.join)

        # Expand df so there is only one pfam per row.
        lens = [len(item) for item in pfam_rules_df.uniprot_enzyme_pfams_list]
        pfams_flat = [n for sub in pfam_rules_df.uniprot_enzyme_pfams_list for n in sub]
        new_df = pd.DataFrame({'uniprot_id': np.repeat(pfam_rules_df.uniprot_id, lens),
                               'pfam_rule': pfams_flat}).drop_duplicates()

        pfam_rules_df = pd.merge(pfam_rules_df, new_df, how='outer')    # on uniprot_id.
        del pfams_flat, uniprot_enzyme_pfams_acc_list,  new_df
        del pfam_rules_df['uniprot_enzyme_pfams_list'], pfam_rules_df['uniprot_enzyme_pfams_acc']
        # ++ pfam_rule, uniprot_enzyme_pfams >>> -- enzyme_pfams_acc

    # MERGE
    gizmos.print_milestone('Merging data...', Options.verbose)
    merged_df = pd.merge(predictions_df, pfam_rules_df, how='inner')    # on reaction_id
    merged_df = pd.merge(merged_df, annotations_df, how='inner')        # on pfam_rule (from pfam_rules)
    del merged_df['pfam_rule'], predictions_df, pfam_rules_df, annotations_df    # memory management
    gizmos.print_milestone('Duplicate cleanup...', Options.verbose)
    merged_df = merged_df.drop_duplicates()         # annotations_df merge produces duplicates due to pfam_rule
    # now each rule has suspect genes

    # TRANSCRIPTOME AND METABOLOME
    if Options.gene_expression_file:        # TODO CHECK.....
        gizmos.print_milestone('Loading transcriptome...', Options.verbose)
        transcripts_df = pd.read_csv(Options.gene_expression_file, index_col=0)
        transcripts_df.index.name = 'gene'
        mask1 = transcripts_df.index.isin(merged_df.gene)
        transcripts_df = transcripts_df[mask1]

        gizmos.print_milestone('Loading metabolome...', Options.verbose)
        metabolome_df = pd.reac_csv(Options.ms_abundance_file, index_col=0)
        metabolome_df.index.name = 'ms_name'
        mask1 = metabolome_df.index.isin(merged_df.ms_substrate)
        mask2 = metabolome_df.index.isin(merged_df.ms_product)
        metabolome_df = metabolome_df[mask1 | mask2]
        # metabolome and transcripts are now filtered to only include gene/ms in the other data.

    # COEXPRESSION
    if Options.coexpression_file:
        gizmos.print_milestone('Loading coexpression...', Options.verbose)
        coexpression_df = pd.read_csv(Options.coexpression_file, index_col=None)
        coexpression_df = coexpression_df.rename(columns={coexpression_df.columns[0]: 'ms_name',
                                                          coexpression_df.columns[1]: 'gene',
                                                          coexpression_df.columns[2]: 'correlation',
                                                          coexpression_df.columns[3]: 'P'})
        mask1 = coexpression_df.gene.isin(merged_df.gene)
        coexpression_df = coexpression_df[mask1]
        mask1 = coexpression_df.ms_name.isin(merged_df.ms_substrate)
        mask2 = coexpression_df.ms_name.isin(merged_df.ms_product)
        coexpression_df = coexpression_df[mask1 | mask2]
        if 'gene_annotation' in coexpression_df.columns:
            del coexpression_df['gene_annotation']
        # coexpression now filtered to only include gene/ms in the other data.

    # MAIN MERGE
    gizmos.print_milestone('Merging data with coexpression...', Options.verbose)
    subs_corr_df = pd.merge(merged_df, coexpression_df.rename(columns={'ms_name': 'ms_substrate',            # on gene
                                                                       'correlation': 'correlation_substrate',
                                                                       'P': 'P_substrate'}), how='inner')
    prod_corr_df = pd.merge(merged_df, coexpression_df.rename(columns={'ms_name': 'ms_product',
                                                                       'correlation': 'correlation_product',
                                                                       'P': 'P_product'}), how='inner')
    del merged_df, coexpression_df

    full_df = pd.merge(subs_corr_df, prod_corr_df, how='inner')
    del subs_corr_df, prod_corr_df
    # Here, results may include same rule-gene-coexp data, but through different RR_enzyme annotation

    gizmos.print_milestone('Outputting...', Options.verbose)
    # FULL OUTPUT
    cols = ['ms_substrate', 'ms_product',
            'expected_mass_transition', 'predicted_mass_transition', 'mass_transition_difference',
            'reaction_id', 'substrate_id', 'product_id',
            'substrate_mnx', 'product_mnx',
            'predicted_substrate_id', 'predicted_product_id', 'predicted_substrate_smiles', 'predicted_product_smiles',
            'smarts_id', 'RR_substrate_smarts', 'RR_product_smarts',
            'uniprot_id', 'uniprot_enzyme_pfams', 'KO',
            'rhea_id_reaction', 'kegg_id_reaction',
            'rhea_confirmation', 'kegg_confirmation', 'KO_prediction',
            'gene', 'enzyme_pfams', 'correlation_substrate',
            'P_substrate', 'correlation_product', 'P_product']
    output_file = os.path.join(Options.output_folder, 'full_results.csv')
    full_df[cols].to_csv(output_file, index=False)

    # UNIQUE PATH
    cols = ['ms_substrate', 'ms_product',
            'predicted_substrate_id', 'predicted_product_id', 'predicted_substrate_smiles', 'predicted_product_smiles',
            'gene', 'enzyme_pfams', 'correlation_substrate',
            'P_substrate', 'correlation_product', 'P_product']
    output_file = os.path.join(Options.output_folder, 'unique_path_results.csv')
    full_df = full_df[cols].drop_duplicates()
    full_df.to_csv(output_file, index=False)

    return


if __name__ == "__main__":
    Options = get_args()
    Options.log_file = os.path.join(Options.output_folder, 'log.txt')
    main()
