#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import sys
import argparse
import re
import pandas as pd
import networkx as nx
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem.Draw import DrawingOptions

import gizmos

DrawingOptions.atomLabelFontSize = 80
DrawingOptions.dotsPerAngstrom = 100
DrawingOptions.bondLineWidth = 2.5


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('structures_file',
                        help='Output from heraldPathways.py')
    parser.add_argument('output_folder')
    parser.add_argument('--reactions_file',
                        default='',
                        required=False,
                        help='Output from heraldPathways.py')
    parser.add_argument('--pfam_RR_annotation_file',
                        default='',
                        required=False,
                        help='Nine-column csv. reaction_id, uniprot_id, Pfams, KO, rhea_id_reaction, kegg_id_reaction, '
                             'rhea_confirmation, kegg_confirmation, KO_prediction.'
                             '\nUsing this input will add to the pathway figures which pfams are capable of each '
                             'reaction.')
    parser.add_argument('--gene_annotation_file',
                        default='',
                        required=False,
                        help='Two-column csv. Gene, pfam1;pfam2'
                             '\nUsing this input in conjunction with pfam_RR_annotation_file will add to the pathway '
                             'figures which uncorrelated genes in the genome are capable of each reaction.')
    parser.add_argument('--pfam_RR_annotation_dataset',
                        default='strict',
                        required=False,
                        choices=['strict', 'medium', 'loose'],
                        help='Default: strict.')
    parser.add_argument('--use_pd_graph',
                        default=True,
                        required=False,
                        choices=[True, False],
                        type=bool,
                        help='Default: True. Use False with older versions of networkx that do not have pandas '
                             'compatability.')
    parser.add_argument('--print_all_molecules', '-m',
                        default=False,
                        action='store_true',
                        required=False,
                        help='Flag. Use to generate SVGs of all molecules in the results.')
    parser.add_argument('--pathway', '-p',
                        default='',
                        required=False,
                        help='Input comma-separated nodes to generate SVGs of a specific pathway.')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        required=False)
    return parser.parse_args()


def get_structure_network(reactions_df):
    structures_network_df = reactions_df[['predicted_substrate_id', 'predicted_product_id']].drop_duplicates().copy()
    if 'correlation_substrate' in reactions_df:
        structures_network_df = structures_network_df.apply(get_structure_network_edge_info, df=reactions_df, axis=1)
    else:
        structures_network_df['genes_corr_substrate'] = 0
        structures_network_df['genes_corr_product'] = 0
        structures_network_df['genes_corr_both'] = 0
    # ++ genes_corr_substrate, genes_corr_product, genes_corr_both

    structures_network = gizmos.df_to_graph(structures_network_df, Options.use_pd_graph)
    return structures_network, structures_network_df


def get_structure_network_attributes(root_id, structures_network, structures_df):
    reaction_distance_df = pd.DataFrame.from_dict(nx.single_source_shortest_path_length(
        structures_network.to_undirected(), root_id), orient='index', columns=['root_distance'])
    structures_attributes_df = pd.merge(structures_df, reaction_distance_df, left_index=True, right_index=True)
    return structures_attributes_df


def get_structure_network_edge_info(cur_edge, df):
    substrate_mask = cur_edge.predicted_substrate_id == df['predicted_substrate_id']
    product_mask = cur_edge.predicted_product_id == df['predicted_product_id']

    # CORRELATED GENES
    cur_edge['genes_corr_substrate'] = len(
        df[substrate_mask & product_mask].dropna(subset=['correlation_substrate']))
    cur_edge['genes_corr_product'] = len(
        df[substrate_mask & product_mask].dropna(subset=['correlation_product']))
    cur_edge['genes_corr_both'] = len(
        df[substrate_mask & product_mask].dropna(subset=['correlation_substrate', 'correlation_product']))

    return cur_edge


def print_svg_molecules(cur_structure, out_folder):
    fname = os.path.join(out_folder, cur_structure.name)
    if not os.path.exists(fname):
        mol = Chem.MolFromSmiles(cur_structure.predicted_substrate_smiles)
        if bool(mol):
            Draw.MolToFile(mol, fname + ".svg")
    return


def get_mol_svg_lines(fname, cur_y):
    y_pattern = re.compile(r'( y[1,2]?=")(\S+)"')

    new_lines = []
    y_list = []

    # GET ACTUAL MOLECULE DIMENSIONS
    with open(fname, 'r') as mf:
        n_line = 0
        for line in mf:
            n_line += 1
            if n_line <= 6:  # header
                continue
            else:
                vals_to_offset = [n for n in y_pattern.finditer(line)]
                if vals_to_offset:
                    for cur_match in vals_to_offset:
                        old_y = float(cur_match[2])
                        y_list.append(old_y)

    top_y = min(y_list)
    bottom_y = max(y_list)

    tight_y_offset = cur_y - top_y

    with open(fname, 'r') as mf:
        n_line = 0
        for line in mf:
            n_line += 1
            if n_line <= 6:  # header
                continue
            if '</svg:svg>' in line:
                continue
            else:
                vals_to_offset = [n for n in y_pattern.finditer(line)]
                if vals_to_offset:
                    start_pos = 0
                    mod_line = ''
                    for cur_match in vals_to_offset:
                        old_y = float(cur_match[2])
                        new_y = str(old_y + tight_y_offset)
                        mod_line += line[start_pos:cur_match.span()[0]]
                        mod_line += cur_match[1]
                        mod_line += new_y
                        mod_line += '"'
                        start_pos = cur_match.span()[1]
                    mod_line += line[vals_to_offset[-1].span()[1]:]
                    new_lines.append(mod_line)
                else:
                    new_lines.append(line)
    return new_lines, bottom_y + tight_y_offset


def print_pathway(nodes, reactions_df, output_file, molecules_folder, print_reaction_data,
                  print_uncorr_pfams=False, print_uncorr_genes=False, enzyme_df=pd.DataFrame()):
    def get_svg_text(string, size, x_coordinate, color='black'):
        svg_text_header = '<svg:text font-family="helvetica"   font-size="'
        svg_text_header2_black = '" fill="rgb(0,0,0)" x="'
        svg_text_header2_red = '" fill="rgb(255,0,0)" x="'
        svg_text_header2_green = '" fill="rgb(0,155,0)" x="'
        svg_text_header2_blue = '" fill="rgb(0,0,255)" x="'
        svg_text_header3 = '" y="'
        svg_text_header4 = '">'
        svg_text_tail = '</svg:text>\n'

        path_svg_lines_genes.append(svg_text_header)
        path_svg_lines_genes.append(str(size))
        if color == 'red':
            path_svg_lines_genes.append(svg_text_header2_red)
        elif color == 'green':
            path_svg_lines_genes.append(svg_text_header2_green)
        elif color == 'blue':
            path_svg_lines_genes.append(svg_text_header2_blue)
        else:   # black
            path_svg_lines_genes.append(svg_text_header2_black)
        path_svg_lines_genes.append(str(x_coordinate))
        path_svg_lines_genes.append(svg_text_header3)
        path_svg_lines_genes.append(str(cur_y))
        path_svg_lines_genes.append(svg_text_header4)
        path_svg_lines_genes.append(string)
        path_svg_lines_genes.append(svg_text_tail)
        return

    def get_svg_arrow(start, end):
        svg_arrow_header = '<svg:line x1="50" y1="'
        svg_arrow_header_2 = '" x2="50" y2="'
        svg_arrow_tail = '" stroke="#000" stroke-width="3" marker-end="url(#arrow)"></svg:line>'

        path_svg_lines_genes.append(svg_arrow_header)
        path_svg_lines_genes.append(str(start - (15/2)))
        path_svg_lines_genes.append(svg_arrow_header_2)
        path_svg_lines_genes.append(str(end - 15))
        path_svg_lines_genes.append(svg_arrow_tail)
        return

    # DEFINITIONS
    svg_header = '''<?xml version="1.0" encoding="iso-8859-1"?>
<svg:svg version="1.1" baseProfile="full"
        xmlns:svg="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        xml:space="preserve" height="'''
    svg_header2 = '''">
<svg:defs>
   <svg:marker id="arrow" markerWidth="10" markerHeight="10" refX="0" refY="2" orient="auto" markerUnits="strokeWidth">
     <svg:path d="M0,0 L0,4 L7,2 z" fill="#000"></svg:path>
   </svg:marker>
</svg:defs>'''
    svg_tail = '</svg:svg>'

    next_mol_y_offset = 30

    # INITIATILIZING
    path_svg_lines_genes = []
    cur_y = 30

    # MAIN LOOP
    for i, cur_mol in enumerate(nodes):
        mol_file = os.path.join(molecules_folder, cur_mol + '.svg')

        # MOL NAME
        get_svg_text(cur_mol, 25, 5)
        cur_y += 25

        # MASS NAME
        substrate_mask = reactions_df.predicted_substrate_id == cur_mol
        product_mask = reactions_df.predicted_product_id == cur_mol
        ms_substrates = reactions_df.ms_substrate[substrate_mask].unique()
        ms_products = reactions_df.ms_product[product_mask].unique()
        ms_substrates = list(set(ms_substrates).union(set(ms_products)))
        ms_substrates = ' | '.join(ms_substrates)
        get_svg_text(ms_substrates, 15, 5)
        cur_y += 15

        # MOL
        mol_lines, cur_y = get_mol_svg_lines(mol_file, cur_y)
        for line in mol_lines:
            path_svg_lines_genes.append(line)
        cur_y += next_mol_y_offset

        # REACTION DATA
        cur_y_genes_start = cur_y
        if print_reaction_data:
            if i == len(nodes) - 1:     # last molecule
                continue
            else:
                product = nodes[i+1]

                substrate_mask = reactions_df.predicted_substrate_id == cur_mol
                product_mask = reactions_df.predicted_product_id == product

                cur_reaction_rows = reactions_df[substrate_mask & product_mask]

                # GENE
                if 'correlation_substrate' in reactions_df:
                    cur_reaction_rows = cur_reaction_rows[
                        ['gene', 'enzyme_pfams', 'correlation_substrate', 'correlation_product']].drop_duplicates()
                    cur_reaction_rows = cur_reaction_rows.sort_values(by=['enzyme_pfams', 'gene'])

                    for cur_gene in cur_reaction_rows.gene.unique():
                        gene_mask = cur_reaction_rows.gene == cur_gene
                        cur_gene_df = cur_reaction_rows[gene_mask]
                        cur_gene_pfams = cur_gene_df.enzyme_pfams.iloc[0]

                        subs_none_corr = cur_gene_df.correlation_substrate.isna()
                        prod_none_corr = cur_gene_df.correlation_product.isna()
                        subs_pos_corr = cur_gene_df.correlation_substrate > 0
                        subs_neg_corr = cur_gene_df.correlation_substrate < 0
                        prod_pos_corr = cur_gene_df.correlation_product > 0
                        prod_neg_corr = cur_gene_df.correlation_product < 0

                        if subs_none_corr.all():                        # no corr
                            pass
                        elif subs_pos_corr.all():                       # green ^
                            get_svg_text('^', 15, 60, color='green')
                        elif subs_neg_corr.all():                       # red ^
                            get_svg_text('^', 15, 60, color='red')
                        else:                                           # blue ^
                            get_svg_text('^', 15, 60, color='blue')

                        if prod_none_corr.all():                        # no corr
                            pass
                        elif prod_pos_corr.all():                       # green v
                            get_svg_text('v', 15, 70, color='green')
                        elif prod_neg_corr.all():                       # red v
                            get_svg_text('v', 15, 70, color='red')
                        else:                                           # blue v
                            get_svg_text('v', 15, 70, color='blue')

                        cur_line = ' | '.join([cur_gene, cur_gene_pfams])
                        get_svg_text(cur_line, 15, 80)
                        cur_y += 15

                elif print_uncorr_pfams:
                    cur_reaction_ids = cur_reaction_rows.reaction_id.unique()
                    reaction_ids_mask = enzyme_df.reaction_id.isin(cur_reaction_ids)
                    cur_reaction_pfams = enzyme_df.uniprot_enzyme_pfams[reaction_ids_mask].unique()
                    if len(cur_reaction_pfams):
                        cur_reaction_pfams = {n for sub in cur_reaction_pfams for n in sub.split(';')}
                        cur_reaction_pfams = sorted(list(cur_reaction_pfams))
                        for cur_pfam in cur_reaction_pfams:
                            get_svg_text(cur_pfam, 15, 60)
                            cur_y += 15
                    else:
                        cur_y += next_mol_y_offset

                elif print_uncorr_genes:
                    cur_reaction_ids = cur_reaction_rows.reaction_id.unique()
                    reaction_ids_mask = enzyme_df.reaction_id.isin(cur_reaction_ids)
                    cur_reaction_genes_df = enzyme_df[reaction_ids_mask]
                    cur_reaction_genes_df = cur_reaction_genes_df[['gene', 'enzyme_pfams']].drop_duplicates()
                    if not cur_reaction_genes_df.empty:
                        cur_reaction_genes_df = cur_reaction_genes_df.sort_values(by=['enzyme_pfams', 'gene'])
                        for j, cur_gene in cur_reaction_genes_df.iterrows():
                            cur_line = ' | '.join([cur_gene.gene, cur_gene.enzyme_pfams])
                            get_svg_text(cur_line, 15, 60)
                            cur_y += 15
                    else:
                        cur_y += next_mol_y_offset

        else:
            cur_y += next_mol_y_offset

        # ARROW
        if i == len(nodes) - 1:  # last molecule
            continue
        else:
            cur_y_genes_end = cur_y
            get_svg_arrow(cur_y_genes_start, cur_y_genes_end)

        cur_y += next_mol_y_offset

    path_svg_lines_genes.append(svg_tail)

    with open(output_file, 'w') as pf:
        pf.write(svg_header)
        pf.write(str(cur_y + 5))
        pf.write(svg_header2)
        for line in path_svg_lines_genes:
            pf.write(line)
    return


def print_all_pathways(pathway_nodes, reactions_df, enzyme_df, structures_df, out_folder):
    structures_df.loc[pathway_nodes].apply(print_svg_molecules, out_folder=Options.svg_folder, axis=1)

    if 'correlation_substrate' in reactions_df:
        fname = os.path.join(out_folder, 'longest_path_root_genes.svg')
        print_pathway(pathway_nodes, reactions_df, fname, Options.svg_folder, True)

    fname = os.path.join(out_folder, 'longest_path_root.svg')
    print_pathway(pathway_nodes, reactions_df, fname, Options.svg_folder, False)

    if Options.print_uncorr_pfams:
        fname = os.path.join(out_folder, 'longest_path_root_uncorr_pfams.svg')
        print_pathway(pathway_nodes, reactions_df, fname, Options.svg_folder, True,
                      print_uncorr_pfams=True, enzyme_df=enzyme_df)
    if Options.print_uncorr_genes:
        fname = os.path.join(out_folder, 'longest_path_root_uncorr_genes.svg')
        print_pathway(pathway_nodes, reactions_df, fname, Options.svg_folder, True,
                      print_uncorr_genes=True, enzyme_df=enzyme_df)
    return


def get_rooted_semiforward_network(structure_network_df, structures_attributes_df):
    """
    Prepares network so
    :param structure_network_df:
    :param structures_attributes_df:
    :return:
    """
    # todo check attributes
    structures_attributes_df2 = structures_attributes_df[['root_distance']]
    structure_network_df = structure_network_df.copy()

    structures_attributes_df2 = structures_attributes_df2.rename(
        columns={'root_distance': 'root_distance_substrate'})
    structure_network_df = pd.merge(structure_network_df, structures_attributes_df2,
                                    left_on='predicted_substrate_id', right_index=True)

    structures_attributes_df2 = structures_attributes_df2.rename(
        columns={'root_distance_substrate': 'root_distance_product'})
    structure_network_df = pd.merge(structure_network_df, structures_attributes_df2,
                                    left_on='predicted_product_id', right_index=True)

    structure_network_df['semiforward_reaction'] = (structure_network_df.root_distance_product >=
                                                    structure_network_df.root_distance_substrate)

    semiforward_network_df = structure_network_df[structure_network_df.semiforward_reaction]
    semiforward_network = gizmos.df_to_graph(semiforward_network_df, True)
    return semiforward_network


#################
# MAIN PIPELINE #
#################
def main():
    # OUTPUT INIT
    gizmos.log_init(Options)
    if not os.path.exists(Options.svg_folder):
        os.makedirs(Options.svg_folder)

    # ms_substrate, predicted_substrate_id, predicted_substrate_mm, predicted_substrate_smiles, InChI, reacted, root
    gizmos.print_milestone('Loading structures...', Options.verbose)
    structures_df = pd.read_csv(Options.structures_file, index_col=0)

    if 'predicted_substrate_mm' not in structures_df:
        structures_df['predicted_substrate_mm'] = structures_df.predicted_substrate_smiles.apply(gizmos.get_mm_from_str,
                                                                                                 is_smarts=False)
    if not Options.reactions_file or Options.print_all_molecules:
        structures_df.apply(print_svg_molecules, out_folder=Options.svg_folder, axis=1)

    if Options.reactions_file:
        gizmos.print_milestone('Loading reactions...', Options.verbose)
        # INPUT
        # ms_substrate, ms_product, expected_mass_transition, predicted_mass_transition, mass_transition_difference,
        #  reaction_id, substrate_id, product_id, substrate_mnx, product_mnx, predicted_substrate_id,
        #   predicted_product_id, predicted_substrate_smiles, predicted_product_smiles, smarts_id, RR_substrate_smarts,
        #    RR_product_smarts, uniprot_id, uniprot_enzyme_pfams, KO, rhea_id_reaction, kegg_id_reaction,
        #     rhea_confirmation, kegg_confirmation, KO_prediction, gene, enzyme_pfams, correlation_substrate,
        #      P_substrate, correlation_product, P_product
        reactions_df = pd.read_csv(Options.reactions_file, index_col=None, dtype={'rhea_id_reaction': str,
                                                                                  'reaction_id': str})
        reactions_df = reactions_df.set_index('root')

        if Options.gene_annotation_file and not Options.pfam_RR_annotation_file:
            sys.exit(
                'ERROR: Gene annotations input is only usable in combination with the pfam_RR_annotation_file input.')
        else:
            enzyme_df = gizmos.load_enzyme_input(Options)
            if Options.pfam_RR_annotation_file:
                Options.print_uncorr_pfams = True
                if Options.gene_annotation_file:
                    Options.print_uncorr_genes = True

        if Options.pathway:
            gizmos.print_milestone('Printing selected pathway...', Options.verbose)
            pathway = Options.pathway.split(',')
            print_all_pathways(pathway, reactions_df, enzyme_df, structures_df, Options.output_folder)
        else:
            gizmos.print_milestone('Generating networks...', Options.verbose)
            structures_network, structure_network_df = get_structure_network(reactions_df)
            fname = os.path.join(Options.output_folder, 'full_structure_network.csv')
            structure_network_df.to_csv(fname, index=False)

            # SUB NETWORKS
            subnetworks = [n for n in nx.connected_components(structures_network.to_undirected())]
            subnetworks_dict = dict()
            for sub_idx, cur_sub in enumerate(subnetworks):
                for node in cur_sub:
                    subnetworks_dict[node] = sub_idx

            # PROCESS EACH ROOT NETWORK
            gizmos.print_milestone('Processing each network...', Options.verbose)
            for cur_root in reactions_df.index.unique():
                cur_root_out_folder = os.path.join(Options.output_folder, 'root_networks', cur_root+'/')
                if not os.path.exists(cur_root_out_folder):
                    os.makedirs(cur_root_out_folder)

                # GET NETWORK COMPONENT WITH ROOT
                cur_root_structures = subnetworks[subnetworks_dict[cur_root]]
                cur_root_structures_network = structures_network.subgraph(cur_root_structures)

                # GET ROOT-SPECIFIC DATA
                cur_root_structures_df = structures_df[structures_df.index.isin(cur_root_structures)].copy()
                subs_mask = structure_network_df.predicted_substrate_id.isin(cur_root_structures)
                prod_mask = structure_network_df.predicted_product_id.isin(cur_root_structures)
                cur_root_structures_network_df = structure_network_df[subs_mask & prod_mask]

                # PREPARE STRUCTURES
                cur_root_structures_df['root_mm'] = cur_root_structures_df.predicted_substrate_mm.loc[cur_root]
                cur_root_structures_df['root_mm_diff'] = cur_root_structures_df.predicted_substrate_mm - \
                                                         cur_root_structures_df.root_mm
                cur_root_structures_df['root_mm_diff_abs'] = cur_root_structures_df.root_mm_diff.apply(abs)

                # GET STRUCTURE NETWORK ATTRIBUTES
                cur_root_structures_attributes_df = get_structure_network_attributes(cur_root, cur_root_structures_network,
                                                                                     structures_df)

                # OUTPUT
                fname = os.path.join(cur_root_out_folder, 'structure_network.csv')
                cur_root_structures_network_df.to_csv(fname, index=False)

                fname = os.path.join(cur_root_out_folder, 'structure_network_attributes.csv')
                cur_root_structures_attributes_df.to_csv(fname, index=True)

                # FORWARD DF
                # todo why?
                semiforward_network = get_rooted_semiforward_network(cur_root_structures_network_df,
                                                                     cur_root_structures_attributes_df)

                # MAKE DAG
                root_dag_structures_network = gizmos.get_dag_from_structures_network(cur_root_structures_network,
                                                                                     cur_root_structures_attributes_df)
                longest_path = nx.dag_longest_path(root_dag_structures_network)  # list of nodes, ordered

                # OUTPUT MOLECULE SVGs
                print_all_pathways(longest_path, reactions_df, enzyme_df, structures_df, cur_root_out_folder)

    return


if __name__ == "__main__":
    Options = get_args()
    Options.log_file = os.path.join(Options.output_folder, 'log.txt')
    Options.print_uncorr_pfams = False
    Options.print_uncorr_genes = False
    Options.correlation_file = ''
    Options.svg_folder = os.path.join(Options.output_folder, 'molecules/')
    main()
