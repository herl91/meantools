# **MEAN**tools
**ME**tabolite **AN**ticipation tools

# OTHER DOWNLOADS

* The MetaNetX database release "chem_prop.tsv", which can be downloaded from their website: https://www.metanetx.org/mnxdoc/mnxref.html
* The RetroRules database version "rr_01" in SQLite (named mvc.db), which can be downloaded from their website: https://retrorules.org/dl
* The Retrorules-Pfam annotations, which have been curated and can be downloaded from here: 
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4603902.svg)](https://doi.org/10.5281/zenodo.4603902)


# WORKFLOW SUMMARY

![alt text](docs/workflow.png)
#### Arrows:
Arrows in this workflow show where the input of which script comes from.

Grey arrows show the simplest workflow: only using metabolome information. 

Red and blue arrows show the additional steps when using transcriptome data as well.
Introducing transcriptomic data acts as a filter: reactions without associated
correlated transcripts are filtered out. Because of this, when using the blue
arrows (recommended), the red arrows are not necessary.

#### Squares:
Blue-colored squares are the DATABASE PREPARATION phase.
These scripts parse the RetroRules database to extract the data pertinent to MEANtools.
**These steps only need to be performed once**: upon downloading the RetroRules database.

Green-colored squares are the OMICS DATA PREPARATION phase.

Red-colored square is a list annotating the data from the RetroRules database and its PFAM predictions.
This can be downloaded from #TODO.

Grey-colored squares are the PREDICTION phase. 

# INPUT DESCRIPTION

Primarily, the prediction requires the following data:

* A CSV with the metabolome data describing the mass/charge ratios to analyze:
```
ms_name,mz
metabolite1,123.3
M400T11,456.6
```
* A CSV with structure predictions for each metabolite/mass_signature in the metabolome.

For this, we have provided a script (**queryMassNPDB**) to query the mass/charge ratio CSV described above in a custom CSV list of molecules (and their monoisomeric mass), or WUR-BIOINFORMATICS' Natural Product Database (NPDB) in sqlite. When using a custom list of molecules, this is the format required:
```
molecule_id,molecule_monoisomeric_mass,SMILES
natural_product_1,70,CCCO
natural_procut_2,180.5,CCCCCCCCNC
```
((Matching mass/charge ratio data with metabolite structures requires a library of ions indicating how they affect the mass/charge ratio of a structure. This is provided within this repo.))

#### Using transcriptome data:
For this the following data is required:

* A CSV with the metabolome abundance of each metabolite/mass_signature (rows) in each sample (columns). 
A header must be included, with each sample being identically named in the transcriptome.
```
name,sample1,sample2
metabolite1,100,200
M400T11,400,500
```
* A CSV with the transcriptome abundance of each locus_tag (rows) in each sample (columns). 
A header must be included, with each sample being identically named in the metabolome.
```
name,sample1,sample2
gene1,55,66
gene2,77,88
```
This used by corrMultiomics to generate a list of correlated metabolite-transcripts pairs.
Alternative to using this script, one may provide a custom CSV with correlations in the following format:
```
metabolite,gene,correlation,P
M400T11,gene1,0.7,0.001
metabolite1,gene1,0.6,0.0001
```

* A CSV with PFAM annotations of the genes in the transcriptome.
This can be integrated with the rest of the data at different steps (see workflow picture and help commands of each script).
The format for these annotations is as follows (note that multiple pfams for a given gene
can be be separated by semicolons ;):
```
gene1,p450
gene2,Transferase
gene3,SQHop_cyclase_C;SQHop_cyclase_N
```

# EXAMPLE DATA

Example data for the workflow (used in this thesis chapter for this work) can be found here:

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4603893.svg)](https://doi.org/10.5281/zenodo.4603893)


# DATABASE PREPARATION

**These steps only need to be performed once**: upon downloading the RetroRules database.

Two inputs are needed for this:

* The MetaNetX database release "chem_prop.tsv", which can be downloaded from their website: https://www.metanetx.org/mnxdoc/mnxref.html
* The RetroRules database version "rr_01" in SQLite (named mvc.db), which can be downloaded from their website: https://retrorules.org/dl

## getMassTransitions.py

This script parses the RetroRules and MetaNetX database to generate a CSV with reaction_ids and their associated mass_transitions.

Using the monoisotopic mass is recommended, with the optional flag: "--monoisotopic_mass"

#### Input:
* MetaNetX (chem_prop.tsv)
* RetroRules (mvc.db)

#### Output:
* CSV of reaction,mass_transition

## treatMassTransitions.py

This script rounds and analyses the mass_transitions generated in previous step to make it compatible with the
mass transitions that will later be identified by rdkit during the virtual molecule generation process.
This script also generates some plots showing the transition degeneracy (answering questions such as how many reactions can be attributed to a specific mass_transition number).

#### Input:
* Output from getMassTransitions.py

#### Output:
* CSV of reaction,mass_transition

## validateRulesWithOrigins.py

This script validates the SMILES in RetroRules reactions with that of the metabolites
in MetaNetX. This is because some SMILES have small differences across the two databases.
This script basically filters out these rules, leaving only those in which the molecular
structures involved are identical as those described by MetaNetX.

Using the optional flag "--use_greedy_rxn" is recommended. This flag will make the validation
tests ignore differences in missing or extra H atoms (valence).

#### Input:
* MetaNetX (chem_prop.tsv) 
* RetroRules (mvc.db)
* Output from getMassTransitions.py

#### Output:
* CSV of validated rules.

## mapBaseRetroRules.py

This script maps the relationships among rules in RR. Because the molecular structures in RR
rules are all described in different diameters (substructures), many of these substructures
are identical across several rules, or are substructures of more complex structures in
other rules. This script identifies and output these relationship to optimize the speed
at which rules are tested with the metabolome data.

For example, this script will identify that a rule involving the substructure C-N-C does not
need to be tested for a specific molecule if a rule involving the substructure C-N was already
tested and was not found in the molecule.

"Base rules": are the reaction rules that describe substructures that cannot be further
decomposed in smaller substructures that are also in RetroRules. Base rules represent step
number one in testing the metabolome data.

"Small rules": are the reaction rules described at their smallest diameter, as found in RR.

#### Input:
* RetroRules (mvc.db)

#### Output:
* CSV of reactions map

# OMICS DATA PREPARATION

## queryMassNPDB.py

This script will query a metabolome described in a CSV table of IDs and associated mass/charge ratios
to produce a list of predicted structures to each ID. The metabolome can be queried in a
custom CSV table of structures (see input descriptions section above), or a NPDB database in SQLite.

#### Input:
* NPDB SQLite database.
* CSV of mass_signatures_name,mass_charge_ratio

#### Output:
* CSV of structure predictions for each mass_signature

## corrMultiOmics.py

This script will correlate the metabolome abundances with the transcriptome abundances,
and return a list of annotated and correlated pairs, according to customizable score,
P-value and MAD thresholds. See input description section above for more information.

#### Input:
* Transcriptome (RPKM)
* Metabolome (mass_signature abundance per sample)

#### Output:
* CSV of correlated mass_signature,transcript

# PREDICTION

## pathMassTransitions.py

This script integrates the metabolome and transcriptome data with the RR and MetaNetX data.
In short, this script filters the mass transitions associated with RR reactions according to
the mass signatures found in the metabolome. In this manner, if the metabolome has no metabolites
with a mass of a 1000, then reactions involving masses of a 1000 are filtered out.

Important optional arguments:

* --ghost: Adds "ghost" mass signatures; these are metabolites that cannot be measured in the
metabolome. Each ghost mass signature is linked to at least two other metabolites that can be measured.

Important optional arguments if using blue arrows (flowchart above):

* --corr_cutoff and --corr_p_cutoff: to filter the correlation input through custom thresholds.
* --pfam_RR_annotation_dataset: to filter the associations between pfams and RR reactions (which are often predictions).

#### Input:
* CSV of mass_signatures,mass_charge_ratio
* Output from treatMassTransitions.py
* [Correlations, output from corrMultiomics]
* [Genome PFAM annotations, CSV]
* [PFAM-RR annotations]

#### Output:
* CSV of allowed mass_signature pathways for reaction prediction.

## heraldPathways.py

This is script is the heart of the prediction process.

This script integrates all data to produce pathway predictions.
Here, all input is integreated, and all results are output as CSV tables that can be
examined in a text editor, EXCEL, cytoscape. However, these results relate to the entire
-omics data used. It is advised to use paveWays.py to further filter out the results and
generate visualizations or filtered tables of predicted pathways that are easier to interpret.

The inputs from the red arrows (in the flowchart above) are not necessary if they have been
used in the blue arrows.

The following optional argument is VERY important:

* --iterations

The script will test structures in separate iterations. If setting this parameter to 1,
then only the input structure (reaction substrates) will be tested, generating an additional set of substructures
(virtual molecules, reaction products) and therefore the longest pathway that (often) will be predicted will be
one-reaction-long. If using iterations=2, then the virtual molecules predicted from the first iterations, will also
be tested, generating a third set of molecules (virtual molecules, reaction products of the second set of molecules).
Therefore, the larger this parameter, the longer the pathways that can be predicted, however, each new iteration
will (likely) result in a larger set of molecules than the previous one, so handle with care according to
computing, memory and time limitation.

**TIP**: Use all structures of an expected pathway as input and select iterations=1. The script will only test one reaction
per structure, but if the script manages to recover/predict the correct reactions, it will result in a predicted pathway
longer than one-reaction-long despite only using iterations=1 (because all input structures should be one-reaction-away
from at least one other input structure).

Important optional arguments if using red arrows (flowchart above):

* --corr_cutoff and --corr_p_cutoff: to filter the correlation input through custom thresholds.
* --pfam_RR_annotation_dataset: to filter the associations between pfams and RR reactions (which are often predictions).

Other important optional arguments:

* --use_substrate_mm_in_file: Recommended. This flag causes the script to use the monoisotopic mass of each input structure as given in the file. If not used, then the mm will be calculated through rdkit, which may be unnessary and computer intensive.
* --only_query_small: With this option, reactions will be tested only at the smallest diameter (not recommended for final results).

#### Input:
* Output from pathMassTransitions.py
* Output from validateRulesWithOrigins.py
* Output from mapBaseRetroRules.py
* CSV of structure predictions [output from queryMassNPDB.py]
* [Correlations, output from corrMultiomics]
* [Genome PFAM annotations, CSV]
* [PFAM-RR annotations]

#### Output:
* CSV of reactions
* CSV of structure predictions
* CSV of structures

# ANALYSIS AND VISUALIZATION

## paveWays.py

This script creates tables and visualizations from the predicted reactions that are easier to interpret by filtering
according to user input.

The mandatory input of this script is the structure predictions of heraldPathways. When using only this input,
this script will generate SVGs of all structures predicted, which can be easily browsed to find interesting predicted
structures and their ID (which can then be used as target for a "--pathway" query with this same script, see below).
When also using a reactions_file, the tool will not print SVGs for all molecules, but can be forced with the optional argument flag: "--print_all_molecules"

Important optional arguments:

* --reactions_file: By also using the reactions output from heraldPathways, one can also output filtered reactions and
their visualizations and all annotated data. These are selected with optional argument "--pathway" described below. If not specifying molecules
to link in a predicted pathway, then the script will find the longest pathways in the network involving each of the initial
inputs of the pipeline.
* --pathway: This will prompt the script to find a pathway that links the metabolites passed in this argument. For example,
using "--pathway metabolite1,metabolite2,metabolite3" will prompt the script to find a pathway in the results that links
all three of these structures.

#### Input:
* CSV of structure predictions [Output from heraldPathways.py]
* CSV of reactions [Output from heraldPathways.py]

#### Output:
A folder with SVGs, and tables describing the predicted pathways, structures and their characteristics.


# OUTSIDE PIPELINE

## wcModules.py

The following script does not belong to the pipeline, but may be used separately to identify coexpression clusters that 
have biosynthetic genes according to PMID:28408660

To use this tool, the clusterone tool (https://paccanarolab.org/cluster-one/) must be installed. The installation folder
must be specified to wcModules.py with the optional argument: "-1 or --clusterone_folder"

The main inputs for this script is a CSV with the transcriptome in RPKM, and a CSV with PFAM annotations of the genes
in the transcriptome. The format of these files is identical to the ones used in the main pipeline and described
above.

Alternative to using the transcriptome, this script allows custom input for each of the steps of the workflow described
in the original literature. The optional argument "--input_type" can be used to specify another input type, the format
of which is described below:

* correlation: CSV listing the gene pairs that are coorelated, and their associated correlation weight.
```
gene1,gene,PCC
geneX,geneY,0.8
geneX,geneM,0.6
```
* MR
```
gene1,gene2,MR
geneX,geneY,0.8
geneX,geneM,0.6
```
* edges
```
gene1,gene2,weight
geneX,geneY,0.8
geneX,geneM,0.6
```
* clusterone: the output of the clusterone tool.

Important optional arguments:
* --plots: The script will plot expression heatmaps for the clusters identified.

#### Input:
* --input_file: Transcriptome (RPKM)
* --annotations_file: A CSV with PFAM annotations of the genes in the transcriptome

#### Output:
* clusterone.annotated.csv: A CSV with the clusterone output, which lists all the coexpression clusters identified in
the transcriptome, the genes that belong to the cluster, their pfam annotations and the metabolite type associated with
the cluster based on the core and tailirong biosynthetic domains of the genes in the cluster (following plantiSMASH rules).
* wcmodules.csv: A CSV describing all the clusters identified, but ONLY those that have both core and tailoring genes.
* plots: folder containing expression heatmaps for all clusters described in wcmodules.csv. The genes in each heatmap
are organized in three sections, from top to bottom: genes encoding scaffold-generating enzymes, genes encoding
tailoring-enzymes, and then the rest of the genes.
