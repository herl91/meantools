#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import argparse
import pandas as pd
from rdkit import Chem
from multiprocessing import Pool

import gizmos


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('metabolites_file',                # ms_name, structure_id, structure_mm, SMILES, [InChI]
                        help='CSV with >=4 columns and header. '
                             'Column 1: mass_signature_ID, column 2: predicted_structure_id, '
                             'column 3: monoisomeric_mass, column 4: predicted_structure_smiles.')
    parser.add_argument('reactions_file',     # reaction_id, substrate_id, diameter, smarts_id, rxn_smarts, validated
                        help='Validated rules, output of validateRulesWithOrigins.py')
    parser.add_argument('map_file',  # smarts_id, smarts_is_in, smarts_has, identity, representative_smarts, is_base
                        help='"base_rules.csv", output of mapBaseRetroRules.py')
    parser.add_argument('output_folder')
    parser.add_argument('--transitions_matches_file',  # todo!!!
                        default='',
                        type=str,
                        required=False,
                        help='Output from pathMassTransitions.py')
    parser.add_argument('--filter_reaction_ids',
                        default='',
                        type=str,
                        required=False,
                        help='Query specific reaction_ids. One column txt. Base rules are always tested regardless.')
    parser.add_argument('--query_all',
                        default=False,
                        action='store_true',
                        required=False,
                        help='Use if we should query all rules instead of only small_rules.')
    parser.add_argument('--threads', '-t',
                        default=4,
                        type=int,
                        required=False)
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        required=False)
    parser.add_argument('--dev',
                        default=False,
                        action='store_true',
                        required=False,
                        help='Developer mode.')
    return parser.parse_args()


def get_next_rules(map_df, all_smarts_passed, all_smarts_tested):
    """
    Gets next rules to query based on "smarts_is_in" (next steps), taking into account "smarts_has" (requirements) and
    which smarts have been queried before.
    :param map_df:
    :param all_smarts_passed:
    :param all_smarts_tested:
    :return:
    """
    rules_passed_mask = map_df.smarts_id.isin(all_smarts_passed)
    rules_passed_df = map_df[rules_passed_mask]

    if len(rules_passed_df.smarts_is_in):
        next_smarts = set.union(*rules_passed_df.smarts_is_in)      # list of sets -> set
    else:
        next_smarts = set()

    # Any smarts with a "smarts_has" need to pass all the "smarts_has" before testing.
    next_df = map_df[map_df.smarts_id.isin(next_smarts)].copy()
    next_df['requirements_met'] = next_df.smarts_has.apply(lambda x: x.issubset(all_smarts_passed))  # bool column
    next_df['to_remove'] = ~next_df.requirements_met                # remove smarts that do not meet requirements
    smarts_to_remove = set(next_df.smarts_id[next_df.to_remove])

    # remove smarts_to_remove, and remove rules that have been tested already
    next_smarts = next_smarts - smarts_to_remove
    next_smarts = next_smarts - all_smarts_tested
    return next_smarts


def filter_df(df, map_df, recent_smarts_passed, all_smarts_passed, all_smarts_tested):
    """
    Given a list of smarts, gets filters and calls "get_next_rules". Also updates all_smarts_passed, for proper usage
    of the next function.
    :param df:
    :param map_df:
    :param recent_smarts_passed:
    :param all_smarts_passed:
    :param all_smarts_tested:
    :return:
    """
    if not recent_smarts_passed:
        # We only enter this if in: -first iteration (query base rules)
        #                           -last iteration (query base rules => they have been queried => nothing to query)
        next_smarts_list = map_df.smarts_id[map_df.is_base]
    else:
        # First update recent_smarts_passed (smarts that passed test) to also have all identicals.
        identical_smarts = map_df.identity[map_df.smarts_id.isin(recent_smarts_passed)]
        if len(identical_smarts):
            identical_smarts = set.union(*identical_smarts)     # this expands list of sets into union set.
        else:
            identical_smarts = set()
        recent_smarts_passed.update(identical_smarts)
        # We update all_smarts_passed and tested
        all_smarts_tested.update(recent_smarts_passed)
        all_smarts_passed.update(recent_smarts_passed)
        # We get the next rules in the map
        next_smarts_list = get_next_rules(map_df, all_smarts_passed, all_smarts_tested)

    mask_to_test = df.smarts_id.isin(next_smarts_list)
    mask_not_tested = ~df.smarts_id.isin(all_smarts_tested)
    all_smarts_tested.update(next_smarts_list)      # Preemptive update, as they will be tested in the next steps.
    return df[mask_to_test & mask_not_tested].copy(), all_smarts_passed, all_smarts_tested


def query_filtered_rxn_db(metabolite, rxn_df, map_df):
    """
    Queries each metabolite(metabolites_df) and finds which smiles(_db_df) are in it.
    :param metabolite:
    :param rxn_df:
    :param map_df:
    :return: list of strings = [results, tests]
    """
    all_smarts_tested_ordered = []

    # Querying base rules
    base_rules_df, all_smarts_passed, all_smarts_tested = filter_df(rxn_df, map_df, set(), set(), set())
    all_smarts_tested_ordered.append(set(base_rules_df.smarts_id))
    rules_passed_mask = base_rules_df.rxn_substrate_mol.apply(lambda x: metabolite.mol.HasSubstructMatch(x))
    # ^^ bool series
    recent_smarts_passed = set(base_rules_df.smarts_id[rules_passed_mask])

    # Querying small rules
    while recent_smarts_passed:
        filtered_df, all_smarts_passed, all_smarts_tested = filter_df(rxn_df, map_df, recent_smarts_passed,
                                                                      all_smarts_passed, all_smarts_tested)
        all_smarts_tested_ordered.append(set(filtered_df.smarts_id))
        rules_passed_mask = filtered_df.rxn_substrate_mol.apply(lambda x: metabolite.mol.HasSubstructMatch(x))
        # ^^ bool series
        recent_smarts_passed = set(filtered_df.smarts_id[rules_passed_mask])

    # Querying all rules
    if Options.query_all:
        # From all_smarts_passed we can find which reaction_substrates to query.
        smarts_ids_mask = rxn_df.smarts_id.isin(all_smarts_passed)
        reaction_substrates_to_query = rxn_df.reaction_substrate[smarts_ids_mask]
        reaction_substrates_to_query_mask = rxn_df.reaction_substrate.isin(reaction_substrates_to_query)
        # We have to remove all smarts that have been tested already.
        all_smarts_untested_mask = ~rxn_df.reaction_id.isin(all_smarts_tested)
        # apply
        filtered_df = rxn_df[reaction_substrates_to_query_mask & all_smarts_untested_mask].copy()
        all_smarts_tested_ordered.append(set(filtered_df.smarts_id))
        rules_passed_mask = filtered_df.rxn_substrate_mol.apply(lambda x: metabolite.mol.HasSubstructMatch(x))
        # ^^ bool df series
        recent_smarts_passed = filtered_df.smarts_id[rules_passed_mask]

    all_smarts_passed.update(recent_smarts_passed)
    all_smarts_tested.update(recent_smarts_passed)

    # Preparing strings for output
    results_df = rxn_df[rxn_df.smarts_id.isin(all_smarts_passed)]
    reaction_substrates = set(results_df.reaction_substrate)
    smarts_ids = set(results_df.smarts_id)

    # results_string = structure_id, reaction_substrates, smarts_ids
    if smarts_ids:
        results_string = ','.join([metabolite.structure_id,
                                   ';'.join(reaction_substrates),
                                   ';'.join(smarts_ids)])
    else:
        results_string = ''

    # tests_string = structure_id, matches, fails, tests (round1:round2:round3)
    smarts_ids_fails = all_smarts_tested - all_smarts_passed
    if smarts_ids_fails:
        tests_string = ','.join([metabolite.structure_id,
                                 ';'.join(smarts_ids),
                                 ';'.join(smarts_ids_fails),
                                 ':'.join([';'.join(n) for n in all_smarts_tested_ordered])])
    else:
        tests_string = ''

    return [results_string, tests_string]


def write_output(results_list):
    """
    Writes output.
    :param results_list:
    :return:
    """
    results_string, tests_string = results_list
    if results_string:
        res_file = os.path.join(Options.output_folder, 'results.csv')
        with open(res_file, 'a') as outfile:
            outfile.write(results_string)
            outfile.write('\n')
    if tests_string:
        tests_file = os.path.join(Options.output_folder, 'tests.csv')
        with open(tests_file, 'a') as outfile:
            outfile.write(tests_string)
            outfile.write('\n')
    return


#################
# MAIN PIPELINE #
#################
def main():
    # OUTPUT FOLDER CREATION & LOG
    gizmos.log_init(Options)

    # LOAD INPUT
    # # Input: metabolites
    # ms_name, structure_id, structure_mm, SMILES, [InChI]
    # ++ mol
    gizmos.print_milestone('Loading input...', Options.verbose)
    metabolites_df = pd.read_csv(Options.metabolites_file, index_col=None)
    metabolites_df.rename(columns={metabolites_df.columns[0]: 'ms_name', metabolites_df.columns[1]: 'structure_id',
                                   metabolites_df.columns[2]: 'structure_mm', metabolites_df.columns[3]: 'SMILES'},
                          inplace=True)
    df_to_mol = metabolites_df[['structure_id', 'SMILES']].copy()       # so we do not mol duplicates
    df_to_mol = df_to_mol.drop_duplicates().reset_index(drop=True)
    df_to_mol['mol'] = df_to_mol.SMILES.apply(Chem.MolFromSmiles)
    gizmos.print_milestone(str(len(df_to_mol))+" unique structures IDs identified and mol'd...", Options.verbose)
    metabolites_df = pd.merge(metabolites_df, df_to_mol)
    df_ms_name_squeezed = metabolites_df.groupby('structure_id')['ms_name'].apply(';'.join).reset_index()
    df_ms_name_squeezed.ms_name = df_ms_name_squeezed.ms_name.apply(gizmos.pd_to_set, separator=';')
    del metabolites_df['ms_name']                           # to facilitate merge
    metabolites_df = metabolites_df.drop_duplicates()       # this makes it one row per strcuture_id
    metabolites_df = pd.merge(metabolites_df, df_ms_name_squeezed)
    metabolites_df = metabolites_df.set_index('structure_id')
    del df_to_mol, df_ms_name_squeezed

    # # Input: map
    # smarts_id, smarts_is_in, smarts_has, identity, representative_smarts, is_base
    gizmos.print_milestone('Reading map...', Options.verbose)
    map_df = pd.read_csv(Options.map_file, index_col=None, dtype={'is_base': bool})
    # # # convert to set
    map_df['smarts_is_in'] = map_df.smarts_is_in.apply(gizmos.pd_to_set, separator=';')
    map_df['smarts_has'] = map_df.smarts_has.apply(gizmos.pd_to_set, separator=';')
    map_df['identity'] = map_df.identity.apply(gizmos.pd_to_set, separator=';')
    # # # convert to sets that ignore identity
    map_df['smarts_is_in'] = map_df.smarts_is_in - map_df.identity
    map_df['smarts_has'] = map_df.smarts_has - map_df.identity

    # # Input: reactions
    # reaction_id, substrate_id, diameter, direction, smarts_id, rxn_smarts, rxn_substrate_mm, validated
    # ++ reaction_substrate, rxn_substrate_str, rxn_substrate_mol
    gizmos.print_milestone('Reading reactions...', Options.verbose)
    reactions_df = pd.read_csv(Options.reactions_file, index_col=None,
                               dtype={'reaction_id': str, 'substrate_id': str,
                                      'rxn_substrate_mm': float, 'validated': bool})
    reactions_df = reactions_df[reactions_df.validated].reset_index(drop=True)  # only validated rules
    reactions_df['reaction_substrate'] = reactions_df.reaction_id + '_' + reactions_df.substrate_id
    reactions_df['rxn_substrate_str'] = reactions_df.rxn_smarts.apply(gizmos.get_subs_string)
    reactions_df['rxn_substrate_mol'] = reactions_df.rxn_substrate_str.apply(Chem.MolFromSmarts)

    # MODIFY BASE MAP ACCORDING TO FILTER
    if Options.filter_reaction_ids:
        gizmos.print_milestone('Applying filter...', Options.verbose)
        filter_reaction_ids = set(pd.read_csv(Options.filter_reaction_ids, index_col=None, header=None, squeeze=True,
                                              dtype=str))
        # Take all smarts_id from the filter
        keep = reactions_df.smarts_id[reactions_df.reaction_id.isin(filter_reaction_ids)]
        # Add base rules smarts_id
        base_smarts_ids = map_df[map_df.is_base].smarts_id
        keep.update(base_smarts_ids)
        # Update map_df
        map_df = map_df[map_df.smarts_id.isin(keep)]
        # smarts_has are requirements. We must remove from requirements any smarts_id not in keep.
        map_df.smarts_has = map_df.smarts_has.apply(lambda x: x.intersection(keep))

    allowed_reactions_per_ms = pd.DataFrame()
    if Options.transitions_matches_file:
        # ms, substrate, product, reaction_id, mass_transition_round, mass_transition, substrate_id, substrate_mnx_id,
        # substrate_mm, product_id, product_mnx_id, product_mm
        gizmos.print_milestone('Checking transitions...', Options.verbose)
        allowed_reactions_per_ms = pd.read_csv(Options.transitions_matches_file, index_col=None,
                                               dtype={'reaction_id': str, 'substrate_id': str})
        allowed_reactions_per_ms['allowed_reaction_substrate'] = allowed_reactions_per_ms.reaction_id + '_' +\
                                                         allowed_reactions_per_ms.substrate_id
        allowed_reactions_per_ms = allowed_reactions_per_ms[['substrate_id', 'allowed_reaction_substrate']]
        allowed_reactions_per_ms = allowed_reactions_per_ms.set_index('substrate_id')

    # QUERY (AND WRITING)
    gizmos.print_milestone('Querying...', Options.verbose)
    with open(os.path.join(Options.output_folder, 'results.csv'), 'w') as outfile:
        outfile.write('structure_id,reaction_substrate,smarts_id_match\n')
    with open(os.path.join(Options.output_folder, 'tests.csv'), 'w') as outfile:
        outfile.write('structure_id,smarts_id_match,smarts_id_fail,smarts_id_tests\n')

    if Options.dev or Options.threads == 1:                     # So we can use IPython easily for dev
        for i, cur_metabolite in metabolites_df.iterrows():
            mm_mask = reactions_df.rxn_substrate_mm <= cur_metabolite.structure_mm
            smaller_substructs_df = reactions_df[mm_mask]
            if Options.transitions_matches_file and cur_metabolite.ms_name.intersection(
                    allowed_reactions_per_ms.index):
                allow_this = allowed_reactions_per_ms.allowed_reaction_substrate.loc[cur_metabolite.ms_name]
                reaction_substrate_mask = smaller_substructs_df.reaction_substrate.isin(allow_this)
                smaller_substructs_df = smaller_substructs_df[reaction_substrate_mask]
            results_list = query_filtered_rxn_db(cur_metabolite, smaller_substructs_df, map_df)
            write_output(results_list)
    else:
        with Pool(processes=Options.threads) as pool:
            for i, cur_metabolite in metabolites_df.iterrows():
                mm_mask = reactions_df.rxn_substrate_mm <= cur_metabolite.structure_mm
                smaller_substructs_df = reactions_df[mm_mask]
                if Options.transitions_matches_file and cur_metabolite.ms_name.intersection(
                        allowed_reactions_per_ms.index):
                    allow_this = allowed_reactions_per_ms.allowed_reaction_substrate.loc[cur_metabolite.ms_name]
                    reaction_substrate_mask = smaller_substructs_df.reaction_substrate.isin(allow_this)
                    smaller_substructs_df = smaller_substructs_df[reaction_substrate_mask]
                pool.apply_async(query_filtered_rxn_db, args=(cur_metabolite, smaller_substructs_df, map_df),
                                 callback=write_output)
            pool.close()
            pool.join()
    gizmos.print_milestone('Success!', Options.verbose)
    return


if __name__ == "__main__":
    Options = get_args()
    Options.log_file = os.path.join(Options.output_folder, 'log.txt')
    main()
