#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import argparse
import os
import numpy as np
import pandas as pd

import gizmos


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('mass_signatures_file',
                        help='Two-column csv with header. One mass signature per line. Format: ms_id,mz,mm')
    parser.add_argument('mass_transitions_file',
                        help='Output from treatMassTansitions.py')
    parser.add_argument('output_file')
    parser.add_argument('-d', '--decimals',
                        required=False,
                        type=int,
                        default=2,
                        help='Number of decimals kept. Default: 2.')
    parser.add_argument('--ghosts', '-g',
                        required=False,
                        default=False,
                        action='store_true',
                        help='Flag. Ghosts mass signatures will be added.')
    parser.add_argument('--pfam_RR_annotation_file',
                        default='',
                        required=False,
                        help='Nine-column csv. reaction_id, uniprot_id, Pfams, KO, rhea_id_reaction, kegg_id_reaction, '
                             'rhea_confirmation, kegg_confirmation, KO_prediction')
    parser.add_argument('--gene_annotation_file',
                        default='',
                        required=False,
                        help='Two-column csv. Gene, pfam1;pfam2')
    parser.add_argument('--correlation_file',
                        default='',
                        required=False,
                        help='Four column CSV: metabolite, gene, correlation_coefficient, P-value')
    parser.add_argument('--pfam_RR_annotation_dataset',
                        default='strict',
                        required=False,
                        choices=['strict', 'medium', 'loose'],
                        help='Default: strict.')
    parser.add_argument('--corr_cutoff', '-c',
                        default=0.7,
                        required=False,
                        type=float,
                        help='Minimum absolute correlation coefficient. Default: 0.7. Use 0 for no cutoff.')
    parser.add_argument('--corr_p_cutoff', '-p',
                        default=0.1,
                        required=False,
                        type=float,
                        help='Maximum P value of correlation. Default: 0.1. Use 1 for no cutoff.')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        required=False)
    return parser.parse_args()


def get_ghosts(masses_df, unique_transitions):
    """
    Identifies ghosts by using each reaction left and right of each mass, and finding matches.
    :param masses_df:
    :param unique_transitions:
    :return:
    """
    # forward reactions
    mm_fwd = np.vstack(masses_df.mm_round.to_numpy()) + unique_transitions
    mm_fwd = np.clip(mm_fwd, 0, None)
    mm_fwd = np.round(mm_fwd, Options.decimals)
    mm_fwd_df = pd.DataFrame(mm_fwd, index=masses_df.index, columns=[str(n) for n in unique_transitions])
    mm_fwd_df = mm_fwd_df.reset_index(drop=False)
    mm_fwd_df = pd.melt(mm_fwd_df, id_vars='ms_name', var_name='mass_transition_round', value_name='ghost_name')
    mm_fwd_df['ghost_name'] = mm_fwd_df.ghost_name.apply(lambda x: 'MM' + str(x))
    mm_fwd_df.rename(columns={'ms_name': 'substrate', 'ghost_name': 'product'}, inplace=True)
    mm_fwd_df = mm_fwd_df.astype({'mass_transition_round': float})
    mm_fwd_df = mm_fwd_df[mm_fwd_df.mass_transition_round > 0]

    # backward reactions
    mm_bwd = np.vstack(masses_df.mm_round.to_numpy()) - unique_transitions
    mm_bwd = np.clip(mm_bwd, 0, None)
    mm_bwd = np.round(mm_bwd, Options.decimals)
    mm_bwd_df = pd.DataFrame(mm_bwd, index=masses_df.index, columns=[str(n) for n in unique_transitions])
    mm_bwd_df = mm_bwd_df.reset_index(drop=False)
    mm_bwd_df = pd.melt(mm_bwd_df, id_vars='ms_name', var_name='mass_transition_round', value_name='ghost_name')
    mm_bwd_df['ghost_name'] = mm_bwd_df.ghost_name.apply(lambda x: 'MM' + str(x))
    mm_bwd_df.rename(columns={'ms_name': 'product', 'ghost_name': 'substrate'}, inplace=True)
    mm_bwd_df = mm_bwd_df.astype({'mass_transition_round': float})
    mm_bwd_df = mm_bwd_df[mm_bwd_df.mass_transition_round > 0]

    # match ghost mass
    mm_fwd_df = mm_fwd_df[mm_fwd_df['product'].isin(mm_bwd_df['substrate'])]
    mm_bwd_df = mm_bwd_df[mm_bwd_df['substrate'].isin(mm_fwd_df['product'])]

    # merge
    ghosts_df = pd.concat([mm_fwd_df, mm_bwd_df], sort=True)
    ghosts_df = ghosts_df.astype({'mass_transition_round': 'float'})
    return ghosts_df[['substrate', 'product', 'mass_transition_round']]


def get_transitions(masses_df, unique_transitions):
    """
    finds transitions that are possible
    :param masses_df:
    :param unique_transitions:
    :return:
    """
    path_transitions = np.vstack(masses_df.mm.to_numpy()) + unique_transitions
    path_transitions = np.round(path_transitions, Options.decimals)
    path_transitions_df = pd.DataFrame(path_transitions, index=masses_df.index, columns=[str(n) for n in
                                                                                         unique_transitions])
    path_transitions_df = path_transitions_df.reset_index(drop=False)
    path_transitions_df = pd.melt(path_transitions_df, id_vars='ms_name', var_name='mass_transition',
                                  value_name='mm_round')
    path_transitions_df.rename(columns={'ms_name': 'substrate'}, inplace=True)

    # keep only products that are substrate by merging with masses
    path_transitions_df = pd.merge(path_transitions_df, masses_df.reset_index(drop=False), how='inner')
    # ^^ on mm_round
    path_transitions_df.rename(columns={'ms_name': 'product'}, inplace=True)
    path_transitions_df = path_transitions_df.astype({'mass_transition': 'float'})
    path_transitions_df['mass_transition_round'] = path_transitions_df.mass_transition.apply(
        lambda x: np.round(x, Options.decimals))

    return path_transitions_df[['substrate', 'product', 'mass_transition_round']].drop_duplicates()


def filter_path_with_corr(path_transitions_df, enzyme_df):
    # merge on reaction_id, gene, and ms_substrate/product
    subs_corr_df = pd.merge(path_transitions_df, enzyme_df.rename(columns={'ms_name': 'substrate',
                                                                           'correlation': 'correlation_substrate',
                                                                           'P': 'P_substrate'}), how='inner')
    prod_corr_df = pd.merge(path_transitions_df, enzyme_df.rename(columns={'ms_name': 'product',
                                                                           'correlation': 'correlation_product',
                                                                           'P': 'P_product'}), how='inner')
    # final merge with outer allows for unilateral coexpression
    return pd.merge(subs_corr_df, prod_corr_df, how='outer')


def filter_transitions_with_corr(transitions_df):
    enzyme_df = gizmos.load_enzyme_input(Options)
    enzyme_df = enzyme_df[['reaction_id', 'ms_name', 'gene']].drop_duplicates()

    # merge on ms_name, reaction_id
    merged_df = pd.merge(enzyme_df, transitions_df)

    # remove unnecessary columns, and duplicate cleanup
    merged_df = merged_df.drop(columns=['gene', 'ms_name']).drop_duplicates()
    return merged_df


#################
# MAIN PIPELINE #
#################
def main():
    # INPUT
    gizmos.print_milestone('Loading data...', Options.verbose)

    masses_df = pd.read_csv(Options.mass_signatures_file, index_col=None, header=0)
    masses_df = masses_df.rename(columns={masses_df.columns[0]: 'ms_name',
                                          masses_df.columns[1]: 'mz',
                                          masses_df.columns[2]: 'mm'}).set_index('ms_name')
    if 'mm_round' not in masses_df:
        masses_df['mm_round'] = round(masses_df.mm, Options.decimals)

    transitions_df = pd.read_csv(Options.mass_transitions_file, index_col=None, dtype={'reaction_id': str})
    transitions_df['mass_transition_round_abs'] = transitions_df.mass_transition_round.apply(abs)

    # MINOR TREATMENTS
    mask = transitions_df.mass_transition_round_abs > 0
    transitions_df = transitions_df[mask]
    del transitions_df['mass_transition_round_abs']

    # FILTER TRANSITIONS WITH CORRELATION
    if Options.pfam_RR_annotation_file and Options.gene_annotation_file and Options.correlation_file:
        transitions_df = filter_transitions_with_corr(transitions_df)

    # NO GHOSTS
    gizmos.print_milestone('Identifying transitions...', Options.verbose)
    # todo use mass_transition or round?
    unique_transitions = transitions_df.mass_transition.unique()
    path_transitions_df = get_transitions(masses_df, unique_transitions)

    # GHOSTS
    if Options.ghosts:
        gizmos.print_milestone('Making ghosts...', Options.verbose)
        path_transitions_df = pd.concat([path_transitions_df, get_ghosts(masses_df, unique_transitions)], sort=True)
        path_transitions_df = path_transitions_df.drop_duplicates()

    gizmos.print_milestone('Solving transitions...', Options.verbose)
    # todo possible memory improvement:
    #  cycle through mass_transition_round on different threads and write down immediately?
    solutions_df = pd.merge(transitions_df, path_transitions_df, how='inner')  # on mass_transition_round

    # OUTPUT
    gizmos.print_milestone('Writing...', Options.verbose)
    cols = ['substrate', 'product', 'reaction_id',
            'mass_transition_round', 'mass_transition',
            'substrate_id', 'substrate_mnx_id', 'substrate_mm',
            'product_id', 'product_mnx_id', 'product_mm']
    solutions_df[cols].to_csv(Options.output_file, index=False)
    return


if __name__ == "__main__":
    Options = get_args()
    Options.adducts_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'docs', 'ESI-MS-adducts.csv')
    main()
