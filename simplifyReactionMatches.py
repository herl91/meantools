#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import argparse
import pandas as pd

import gizmos


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('reaction_predictions_file',
                        help='Output from queryReactions.py')
    parser.add_argument('rules_file',
                        help='Output from validateRulesWithOrigins.py')
    parser.add_argument('output_file')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        required=False)
    return parser.parse_args()


#################
# MAIN PIPELINE #
#################
def main():
    """
    converts reaction results to structure_id,reaction_substrate,smarts ... one match per line.
    :return:
    """
    # INPUT
    # RR db (validated rules)
    # reaction_id, substrate_id, diameter, direction, smarts_id, rxn_smarts, validated
    # ++ rule_substrate_str, rule_product_str, reaction_substrate
    gizmos.print_milestone('Loading rules...', Options.verbose)
    rules_df = pd.read_csv(Options.rules_file, index_col=None, dtype={'reaction_id': str, 'substrate_id': str,
                                                                      'validated': bool, 'diameter': int})
    rules_df = rules_df[rules_df.validated]
    rules_df['substrate_str'] = rules_df.rxn_smarts.apply(gizmos.get_subs_string)
    rules_df['product_str'] = rules_df.rxn_smarts.apply(gizmos.get_prod_string)
    rules_df['reaction_substrate'] = rules_df.reaction_id + '_' + rules_df.substrate_id

    gizmos.print_milestone('Simplifying...', Options.verbose)
    with open(Options.output_file, 'w') as outfile:
        outfile.write('structure_id,reaction_substrate,smarts_id,diameter\n')
        with open(Options.reaction_predictions_file, 'r') as reactions:
            for line in reactions:
                # Pre-processing
                structure_id, reaction_substrate, smarts_id_match = line.rstrip().split(',')
                reaction_substrate = gizmos.string_to_set(reaction_substrate, separator=';')
                smarts_id_match = gizmos.string_to_set(smarts_id_match, separator=';')

                # Processing
                mask1 = rules_df.reaction_substrate.isin(reaction_substrate)
                mask2 = rules_df.smarts_id.isin(smarts_id_match)
                cur_rules = rules_df[mask1 & mask2].copy()
                cur_rules = cur_rules.sort_values(by='diameter', ascending=True)
                cur_rules.drop_duplicates(subset='reaction_substrate', keep='last',
                                          inplace=True)  # last = biggest diameter
                for i, rule_row in cur_rules.iterrows():
                    outline = ','.join([structure_id, rule_row.reaction_substrate,
                                        rule_row.smarts_id, str(rule_row.diameter)])
                    outfile.write(outline)
                    outfile.write('\n')
    gizmos.print_milestone('Success!', Options.verbose)
    return


if __name__ == "__main__":
    Options = get_args()
    main()
